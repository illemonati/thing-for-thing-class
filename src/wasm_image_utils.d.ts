/* tslint:disable */
/* eslint-disable */
/**
* @param {number} point_x 
* @param {number} point_y 
* @param {number} tolerence 
* @param {Uint8Array} image_data 
* @param {Uint8Array} out_data 
* @param {number} length 
* @param {number} image_width 
* @param {number} image_height 
* @param {number} r 
* @param {number} g 
* @param {number} b 
*/
export function flood_fill(point_x: number, point_y: number, tolerence: number, image_data: Uint8Array, out_data: Uint8Array, length: number, image_width: number, image_height: number, r: number, g: number, b: number): void;
/**
* @param {number} point_x 
* @param {number} point_y 
* @param {number} tolerence 
* @param {Uint8Array} image_data 
* @param {Uint8Array} out_data 
* @param {number} length 
* @param {number} image_width 
* @param {number} image_height 
* @param {number} r 
* @param {number} g 
* @param {number} b 
*/
export function cool_fill(point_x: number, point_y: number, tolerence: number, image_data: Uint8Array, out_data: Uint8Array, length: number, image_width: number, image_height: number, r: number, g: number, b: number): void;
/**
* @param {Uint16Array} in_hsv_array 
* @param {Uint16Array} out_hsv_array 
* @param {number} multiplier 
* @param {number} range 
* @param {number} array_len 
*/
export function adjust_image_brightness_in_hsv_wasm(in_hsv_array: Uint16Array, out_hsv_array: Uint16Array, multiplier: number, range: number, array_len: number): void;
/**
* @param {Uint16Array} in_hsv_array 
* @param {Uint16Array} out_hsv_array 
* @param {number} hue_multiplier 
* @param {number} hue_range 
* @param {number} saturation_multiplier 
* @param {number} saturation_range 
* @param {number} value_multipler 
* @param {number} value_range 
* @param {number} array_len 
*/
export function adjust_image_hsv_wasm(in_hsv_array: Uint16Array, out_hsv_array: Uint16Array, hue_multiplier: number, hue_range: number, saturation_multiplier: number, saturation_range: number, value_multipler: number, value_range: number, array_len: number): void;
/**
* @param {Uint16Array} in_hsv_array 
* @param {Uint16Array} out_hsv_array 
* @param {number} multiplier 
* @param {number} range 
* @param {number} array_len 
*/
export function adjust_image_saturation_in_hsv_wasm(in_hsv_array: Uint16Array, out_hsv_array: Uint16Array, multiplier: number, range: number, array_len: number): void;
/**
* @param {Uint16Array} in_hsv_array 
* @param {Uint8Array} out_rgb_array 
* @param {number} len 
*/
export function convert_hsv_to_rgb_wasm(in_hsv_array: Uint16Array, out_rgb_array: Uint8Array, len: number): void;
/**
* @param {Uint8Array} in_rgb_array 
* @param {Uint16Array} out_hsv_array 
* @param {number} len 
*/
export function convert_rgb_to_hsv_wasm(in_rgb_array: Uint8Array, out_hsv_array: Uint16Array, len: number): void;
/**
* @param {number} color 
* @param {Uint8Array} data 
* @param {number} len 
* @returns {Uint32Array} 
*/
export function get_pixel_count_for_color(color: number, data: Uint8Array, len: number): Uint32Array;
/**
* @param {Uint8Array} data 
* @param {number} len 
* @returns {MostUsedColor} 
*/
export function get_most_used_color(data: Uint8Array, len: number): MostUsedColor;
/**
* @param {Uint8Array} in_rgb_array 
* @param {Uint8Array} out_rgb_array 
* @param {number} len 
* @param {number} ev_change 
*/
export function change_image_exposure(in_rgb_array: Uint8Array, out_rgb_array: Uint8Array, len: number, ev_change: number): void;
/**
*/
export class MostUsedColor {
  free(): void;
  readonly get_color: Uint8Array;
  times: number;
}

/**
* If `module_or_path` is {RequestInfo}, makes a request and
* for everything else, calls `WebAssembly.instantiate` directly.
*
* @param {RequestInfo | BufferSource | WebAssembly.Module} module_or_path
*
* @returns {Promise<any>}
*/
export default function init (module_or_path?: RequestInfo | BufferSource | WebAssembly.Module): Promise<any>;
        