const rgbToHsv = async (r: number, g: number, b: number) => {
    const v = Math.max(r, g, b);
    const c = v - Math.min(r, g, b);
    let vars = [0, 0, 0];
    switch (v) {
        case r:
            vars = [0, g, b];
            break;
        case g:
            vars = [2, b, r];
            break;
        case b:
            vars = [4, r, g];
            break;
    }
    const h = c === 0 ? 0 : 60 * (vars[0] + (vars[1] - vars[2]) / c);
    const s = v === 0 ? 0 : c / v;
    return [h, s, v];
};

const applyHTransform = (h: number, multiplier: number, range: number) => {
    const hnew = ((multiplier + range / 2) / (range / 2)) * h;
    return hnew % 360;
};

const applyVTransform = (v: number, multiplier: number, range: number) => {
    const vnew = ((multiplier + range / 2) / (range / 2)) * v;
    // const vnew = v + multiplier;
    return vnew <= 1 ? vnew : 1;
};

const applySTransform = (s: number, multiplier: number, range: number) => {
    const snew = ((multiplier + range / 2) / (range / 2)) * s;
    // const snew = s + multiplier;
    return snew <= 1 ? snew : 1;
};

const hsvToRgb = async (h: number, s: number, v: number) => {
    const c = v * s;
    const hPrime = h / 60;
    const x = c * (1 - Math.abs((hPrime % 2) - 1));
    const [r1, g1, b1] =
        hPrime > 5 && hPrime <= 6
            ? [c, 0, x]
            : hPrime > 4
            ? [x, 0, c]
            : hPrime > 3
            ? [0, x, c]
            : hPrime > 2
            ? [0, c, x]
            : hPrime > 1
            ? [x, c, 0]
            : hPrime >= 0
            ? [c, x, 0]
            : [0, 0, 0];
    const m = v - c;
    const [r, g, b] = [r1 + m, g1 + m, b1 + m];
    return [r, g, b];
};

const adjustPixelHSV = async (
    h: number,
    s: number,
    v: number,
    hMultiplier: number,
    hRange: number,
    sMultiplier: number,
    sRange: number,
    vMultiplier: number,
    vRange: number
) => {
    const hnew = applyHTransform(h, hMultiplier, hRange);
    const vnew = applyVTransform(v, vMultiplier, vRange);
    const snew = applySTransform(s, sMultiplier, sRange);
    return [hnew, snew, vnew];
};

export {
    adjustPixelHSV,
    rgbToHsv,
    hsvToRgb,
    applyHTransform,
    applySTransform,
    applyVTransform,
};
