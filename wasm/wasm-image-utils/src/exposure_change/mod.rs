use std::cmp;
use std::f32;
use wasm_bindgen::prelude::*;

#[wasm_bindgen]
pub fn change_image_exposure(
    in_rgb_array: &[u8],
    out_rgb_array: &mut [u8],
    len: usize,
    ev_change: f32,
) {
    for i in (0..len).step_by(4) {
        let r = in_rgb_array[i];
        let g = in_rgb_array[i + 1];
        let b = in_rgb_array[i + 2];
        let (new_r, new_g, new_b) = change_pixel_exposure(r, g, b, ev_change);
        out_rgb_array[i] = new_r;
        out_rgb_array[i + 1] = new_g;
        out_rgb_array[i + 2] = new_b;
    }
}

// This function uses 255 for max rgb
fn change_pixel_exposure(r: u8, g: u8, b: u8, ev_change: f32) -> (u8, u8, u8) {
    // if ev_change == 0f32 {
    //     return (r, g, b);
    // }
    // let rgb_max = max(max(r, g), b);
    let rgb_max = multiple_max!(r, g, b);
    let mut rgb = [r, g, b];
    for i in 0..3 {
        let multiplied = ((rgb[i] as f32) * f32::powf(2f32, ev_change as f32)) as usize;
        if multiplied > 255 {
            rgb[i] = 255;
            continue;
        }
        rgb[i] = cmp::max(multiplied as u8, rgb_max);
    }
    let (r, g, b) = (rgb[0], rgb[1], rgb[2]);
    return (r, g, b);
}
