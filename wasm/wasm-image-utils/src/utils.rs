pub fn set_panic_hook() {
    // When the `console_error_panic_hook` feature is enabled, we can call the
    // `set_panic_hook` function at least once during initialization, and then
    // we will get better error messages if our code ever panics.
    //
    // For more details see
    // https://github.com/rustwasm/console_error_panic_hook#readme
    #[cfg(feature = "console_error_panic_hook")]
    console_error_panic_hook::set_once();
}


#[macro_export]
macro_rules! multiple_max {
    ($x: expr) => ($x);
    ($x: expr, $($y: expr),+) => {
        {
            let a = multiple_max!($($y),+);
            if $x > a {
                $x
            } else {
                a
            }
        }
    }
}

#[macro_export]
macro_rules! multiple_min {
    ($x: expr) => ($x);
    ($x: expr, $($y: expr),+) => {
        {
            let a = multiple_min!($($y),+);
            if $x < a {
                $x
            } else {
                a
            }
        }
    }
}

#[macro_export]
macro_rules! square {
    ($x: expr) => ($x * $x);
}

