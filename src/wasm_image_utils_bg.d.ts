/* tslint:disable */
/* eslint-disable */
export const memory: WebAssembly.Memory;
export function flood_fill(a: number, b: number, c: number, d: number, e: number, f: number, g: number, h: number, i: number, j: number, k: number, l: number, m: number): void;
export function cool_fill(a: number, b: number, c: number, d: number, e: number, f: number, g: number, h: number, i: number, j: number, k: number, l: number, m: number): void;
export function adjust_image_brightness_in_hsv_wasm(a: number, b: number, c: number, d: number, e: number, f: number, g: number): void;
export function adjust_image_hsv_wasm(a: number, b: number, c: number, d: number, e: number, f: number, g: number, h: number, i: number, j: number, k: number): void;
export function adjust_image_saturation_in_hsv_wasm(a: number, b: number, c: number, d: number, e: number, f: number, g: number): void;
export function convert_hsv_to_rgb_wasm(a: number, b: number, c: number, d: number, e: number): void;
export function convert_rgb_to_hsv_wasm(a: number, b: number, c: number, d: number, e: number): void;
export function get_pixel_count_for_color(a: number, b: number, c: number, d: number, e: number): void;
export function __wbg_mostusedcolor_free(a: number): void;
export function __wbg_get_mostusedcolor_times(a: number): number;
export function __wbg_set_mostusedcolor_times(a: number, b: number): void;
export function mostusedcolor_get_color(a: number, b: number): void;
export function get_most_used_color(a: number, b: number, c: number): number;
export function change_image_exposure(a: number, b: number, c: number, d: number, e: number, f: number): void;
export function __wbindgen_malloc(a: number): number;
export function __wbindgen_free(a: number, b: number): void;
export function __wbindgen_realloc(a: number, b: number, c: number): number;
