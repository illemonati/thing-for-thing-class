const createHistogram = async (imageCanvasData, canvas) => {
    const divs = document.getElementsByClassName('hist');
    for (let i = 0; i < divs.length; i++) {
        divs.item(i).innerHTML = '';
    }
    // Extract Pixels
    const ctx = canvas.getContext('2d');
    const imageData = ctx.getImageData(imageCanvasData.x, imageCanvasData.y, imageCanvasData.w, imageCanvasData.h);
    const mostUsedColorDiv = document.getElementById('mostUsedColor');
    mostUsedColorDiv.innerHTML = '';
    imageCanvasData
        .getPixelCountForColor(0)
        .then(redhist => drawHistogram(redhist, 'red'));
    imageCanvasData
        .getPixelCountForColor(1)
        .then(greenhist => drawHistogram(greenhist, 'green'));
    imageCanvasData
        .getPixelCountForColor(2)
        .then(bluehist => drawHistogram(bluehist, 'blue'));
    updateMostUsedColorDiv(mostUsedColorDiv, imageCanvasData);
};
const updateMostUsedColorDiv = async (mostUsedColorDiv, imageCanvasData) => {
    let mostUsed = await imageCanvasData.getMostUsedColor();
    mostUsedColorDiv.innerHTML = `
        <p>Most used color : ${mostUsed.get_color}</p>
        <p>Used : ${mostUsed.times} times </p>
    `;
};
const drawHistogram = async (hist, color) => {
    const histContainer = document.querySelector(`.hist.${color}`);
    const histContainerSize = histContainer.getBoundingClientRect();
    // @ts-ignore
    const max = Math.max(...hist);
    const ht = histContainerSize.height / 255;
    hist.forEach((hist_val, i) => {
        const div = document.createElement('div');
        div.classList.add(`${color}-hist`);
        div.style.backgroundColor = color;
        div.style.height = ht + 'px';
        div.style.width = `${(hist_val / max) * 100}%`;
        histContainer.appendChild(div);
    });
};
export { drawHistogram, updateMostUsedColorDiv, createHistogram };
