use wasm_bindgen::prelude::*;
use std::f32;

fn hsv_to_rgb(h: f32, s: f32, v: f32) -> (f32, f32, f32) {
    let c = v * s;
    let h_prime = h / 60.0;
    let x = c * (1.0 - f32::abs((h_prime % 2.0) - 1.0));
    let (r1, g1, b1) = if (h_prime > 5.0) && h_prime <= 6.0 {
        (c, 0.0, x)
    } else if h_prime > 4.0 {
        (x, 0.0, c)
    } else if h_prime > 3.0 {
        (0.0, x, c)
    } else if h_prime > 2.0 {
        (0.0, c, x)
    } else if h_prime > 1.0 {
        (x, c, 0.0)
    } else if h_prime >= 0.0 {
        (c, x, 0.0)
    } else {
        (0.0, 0.0, 0.0)
    };

    let m = v - c;
    (r1 + m, g1 + m, b1 + m)
}

fn rgb_to_hsv(r: f32, g: f32, b: f32) -> (f32, f32, f32) {
    let v = multiple_max!(r, g, b);
    let c = v - multiple_min!(r, g, b);
    let vars = match v {
        x if x == r => [0.0, g, b],
        x if x == g => [2.0, b, r],
        x if x == b => [4.0, r, g],
        _ => [0.0, 0.0, 0.0],
    };
    let h = if c == 0.0 {
        0.0
    } else {
        60.0 * (vars[0] + ((vars[1] - vars[2]) / c))
    };
    let s = if v == 0.0 { 0.0 } else { c / v };
    (h, s, v)
}

#[wasm_bindgen]
pub fn convert_hsv_to_rgb_wasm(in_hsv_array: &[u16], out_rgb_array: &mut [u8], len: usize) {
    for i in (0..len).step_by(4) {
        let h = in_hsv_array[i] as f32;
        let s = in_hsv_array[i + 1] as f32;
        let v = in_hsv_array[i + 2] as f32;
        let (r, g, b) = hsv_to_rgb(h, s / 100.0, v / 100.0);
        out_rgb_array[i] =   f32::round(r * 255.0) as u8;
        out_rgb_array[i + 1] = f32::round(g * 255.0) as u8;
        out_rgb_array[i + 2] = f32::round(b * 255.0)as u8;
    }
}

#[wasm_bindgen]
pub fn convert_rgb_to_hsv_wasm(in_rgb_array: &[u8], out_hsv_array: &mut [u16], len: usize) {
    // utils::set_panic_hook();
    for i in (0..len).step_by(4) {
        let r = in_rgb_array[i] as f32;
        let g = in_rgb_array[i + 1] as f32;
        let b = in_rgb_array[i + 2] as f32;
        let (h, s, v) = rgb_to_hsv(r / 255.0, g / 255.0, b / 255.0);
        out_hsv_array[i] = h as u16;
        out_hsv_array[i + 1] = f32::round(s * 100.0) as u16;
        out_hsv_array[i + 2] = f32::round(v * 100.0) as u16;
    }
}
