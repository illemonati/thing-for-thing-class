import ImageCanvasData from './imagecanvasdata.js';
import { createHistogram } from './histogram.js';

class Point {
    x: number;
    y: number;
    color: Array<number>;
    fill_color: Array<number>;
    imageData: ImageData;
    fill: boolean;
    tolerance: number;
    constructor(
        x: number,
        y: number,
        imageData: ImageData,
        col?: Array<number>,
        tolerance?: number
    ) {
        this.x = x;
        this.y = y;
        this.imageData = imageData;
        this.color = this.getRgb();
        this.fill_color = col || this.color;
        this.tolerance = tolerance || 100;
        this.fill = this.withinTolerance(this.fill_color);
    }

    getNeightbors() {
        const neighbors = [
            [
                this.getOffsetPoint(-1, -1),
                this.getOffsetPoint(0, -1),
                this.getOffsetPoint(1, -1),
            ],
            [this.getOffsetPoint(-1, 0), this.getOffsetPoint(1, 1)],
            [
                this.getOffsetPoint(-1, 1),
                this.getOffsetPoint(0, 1),
                this.getOffsetPoint(1, 1),
            ],
        ];

        return neighbors;
    }

    withinTolerance(color: Array<number>) {
        const dist =
            (color[0] - this.color[0]) ** 2 +
            (color[1] - this.color[1]) ** 2 +
            (color[2] - this.color[2]) ** 2;
        const fill = dist < this.tolerance;
        return fill;
    }

    getOffsetPoint(dx: number, dy: number) {
        return new Point(
            this.x + dx,
            this.y + dy,
            this.imageData,
            this.fill_color
        );
    }

    getRgb() {
        const index = (this.y * this.imageData.width + this.x) * 4;
        return [
            this.imageData.data[index],
            this.imageData.data[index + 1],
            this.imageData.data[index + 2],
        ];
    }

    setRgb(rgb: Array<number>) {
        const index = (this.y * this.imageData.width + this.x) * 4;
        this.imageData.data[index] = rgb[0];
        this.imageData.data[index + 1] = rgb[1];
        this.imageData.data[index + 2] = rgb[2];
    }

    toString() {
        return `(${this.x}, ${this.y})`;
    }
}

const initFloodFill = async (
    imageCanvasData: ImageCanvasData,
    canvasElement: HTMLCanvasElement
) => {
    const pixelSelectedDiv = document.getElementById(
        'pixelSelected'
    )! as HTMLDivElement;
    canvasElement.onmousedown = (e: MouseEvent) => {
        // const points_to_fill = Array<Point>();
        const ctx = canvasElement.getContext('2d')!;
        let imageData = ctx.getImageData(
            0,
            0,
            canvasElement.width,
            canvasElement.height
        );

        let tolerance = parseInt(
            (pixelSelectedDiv.querySelector(
                '#tolerenceInput'
            )! as HTMLInputElement).value
        );
        if (tolerance < 0) tolerance = 0;

        const coolFill = (pixelSelectedDiv.querySelector(
            '#coolFillInput'
        )! as HTMLInputElement).checked;

        let newR = parseInt(
            (pixelSelectedDiv.querySelector('#newRInput')! as HTMLInputElement)
                .value
        );
        if (newR > 255) newR = 255;
        if (newR < 0) newR = 0;
        let newG = parseInt(
            (pixelSelectedDiv.querySelector('#newGInput')! as HTMLInputElement)
                .value
        );
        if (newG > 255) newG = 255;
        if (newG < 0) newG = 0;
        let newB = parseInt(
            (pixelSelectedDiv.querySelector('#newBInput')! as HTMLInputElement)
                .value
        );
        if (newB > 255) newB = 255;
        if (newB < 0) newB = 0;

        if (coolFill) {
            imageCanvasData
                .coolFill(
                    e.x - imageCanvasData.x,
                    e.y - imageCanvasData.y,
                    tolerance,
                    newR,
                    newG,
                    newB
                )
                .then(async () => {
                    ctx.putImageData(
                        imageCanvasData.imageData,
                        imageCanvasData.x,
                        imageCanvasData.y
                    );
                    await new Promise(r => setTimeout(r, 1));
                    await createHistogram(imageCanvasData, canvasElement);
                });
        } else {
            imageCanvasData
                .floodFill(
                    e.x - imageCanvasData.x,
                    e.y - imageCanvasData.y,
                    tolerance,
                    newR,
                    newG,
                    newB
                )
                .then(async () => {
                    ctx.putImageData(
                        imageCanvasData.imageData,
                        imageCanvasData.x,
                        imageCanvasData.y
                    );
                    await new Promise(r => setTimeout(r, 1));
                    await createHistogram(imageCanvasData, canvasElement);
                });
        }

        const p = new Point(e.x, e.y, imageData);
        updatePixelSelectedDiv(p, imageData, pixelSelectedDiv);
    };
};

const floodFill = async (point: Point, color: Array<number>) => {
    let neighbors = [point];
    point.setRgb(color);
    while (neighbors.length > 0) {
        let new_neightbors = Array<Point>();
        neighbors.forEach(n => {
            let nn = n.getNeightbors();

            nn.forEach(nl => {
                nl.forEach(n => {
                    if (n.fill && n.getRgb() !== color) {
                        n.setRgb(color);
                        new_neightbors.push(n);
                    }
                });
            });
        });
        neighbors = new_neightbors;
    }
};

const updatePixelSelectedDiv = (
    point: Point,
    imageData: ImageData,
    pixelSelectedDiv: HTMLDivElement
) => {
    const position = pixelSelectedDiv.querySelector('#position')!;
    const color = pixelSelectedDiv.querySelector('#color')!;
    position.innerHTML = `${point}`;
    color.innerHTML = `${point.getRgb()}`;
};

export { updatePixelSelectedDiv, floodFill, initFloodFill, Point };
