use wasm_bindgen::prelude::*;
use std::f32;

fn apply_v_transform(v: f32, multiplier: f32, range: f32) -> f32 {
    let vnew = ((multiplier + range / 2.0) / (range / 2.0)) * v;
    return if vnew <= 1.0 { vnew } else { 1.0 };
}

fn apply_s_transform(s: f32, multiplier: f32, range: f32) -> f32 {
    let snew = ((multiplier + range / 2.0) / (range / 2.0)) * s;
    return if snew <= 1.0 { snew } else { 1.0 };
}

fn apply_h_transform(h: f32, multiplier: f32, range: f32) -> f32 {
    let hnew = ((multiplier + range / 2.0) / (range / 2.0)) * h;
    return hnew % 360.0;
}

fn adjust_pixel_brightness_in_hsv(
    h: f32,
    s: f32,
    v: f32,
    multiplier: f32,
    range: f32,
) -> (f32, f32, f32) {
    let vnew = apply_v_transform(v, multiplier, range);
    (h, s, vnew)
}

fn adjust_pixel_saturation_in_hsv(
    h: f32,
    s: f32,
    v: f32,
    multiplier: f32,
    range: f32,
) -> (f32, f32, f32) {
    let snew = apply_s_transform(s, multiplier, range);
    (h, snew, v)
}

fn adjust_pixel_hsv(
    h: f32,
    s: f32,
    v: f32,
    hue_multiplier: f32,
    hue_range: f32,
    saturation_multiplier: f32,
    saturation_range: f32,
    value_multipler: f32,
    value_range: f32,
) -> (f32, f32, f32) {
    let vnew = apply_v_transform(v, value_multipler, value_range);
    let snew = apply_s_transform(s, saturation_multiplier, saturation_range);
    let hnew = apply_h_transform(h, hue_multiplier, hue_range);
    (hnew, snew, vnew)
}

#[wasm_bindgen]
pub fn adjust_image_brightness_in_hsv_wasm(
    in_hsv_array: &[u16],
    out_hsv_array: &mut [u16],
    multiplier: f32,
    range: f32,
    array_len: usize,
) {
    for i in (0..array_len).step_by(4) {
        let h = in_hsv_array[i] as f32;
        let s = in_hsv_array[i + 1] as f32;
        let v = in_hsv_array[i + 2] as f32;
        let (_h_new, _s_new, v_new) =
            adjust_pixel_brightness_in_hsv(h, s / 100.0, v / 100.0, multiplier, range);
        out_hsv_array[i] = f32::round(h) as u16;
        out_hsv_array[i + 1] = f32::round(s) as u16;
        out_hsv_array[i + 2] = (f32::round(v_new) * 100.0) as u16;
    }
}

#[wasm_bindgen]
pub fn adjust_image_hsv_wasm(
    in_hsv_array: &[u16],
    out_hsv_array: &mut [u16],
    hue_multiplier: f32,
    hue_range: f32,
    saturation_multiplier: f32,
    saturation_range: f32,
    value_multipler: f32,
    value_range: f32,
    array_len: usize,
) {
    for i in (0..array_len).step_by(4) {
        let h = in_hsv_array[i] as f32;
        let s = in_hsv_array[i + 1] as f32;
        let v = in_hsv_array[i + 2] as f32;
        let (h_new, s_new, v_new) = adjust_pixel_hsv(
            h,
            s / 100.0,
            v / 100.0,
            hue_multiplier,
            hue_range,
            saturation_multiplier,
            saturation_range,
            value_multipler,
            value_range,
        );
        out_hsv_array[i] = f32::round(h_new) as u16;
        out_hsv_array[i + 1] = f32::round(s_new * 100.0) as u16;
        out_hsv_array[i + 2] = f32::round(v_new * 100.0) as u16;
    }
}

#[wasm_bindgen]
pub fn adjust_image_saturation_in_hsv_wasm(
    in_hsv_array: &[u16],
    out_hsv_array: &mut [u16],
    multiplier: f32,
    range: f32,
    array_len: usize,
) {
    for i in (0..array_len).step_by(4) {
        let h = in_hsv_array[i] as f32;
        let s = in_hsv_array[i + 1] as f32;
        let v = in_hsv_array[i + 2] as f32;
        let (_h_new, s_new, _v_new) =
            adjust_pixel_saturation_in_hsv(h, s / 100.0, v / 100.0, multiplier, range);
        out_hsv_array[i] = f32::round(h) as u16;
        out_hsv_array[i + 1] = f32::round(s_new * 100.0) as u16;
        out_hsv_array[i + 2] = f32::round(v) as u16;
    }
}
