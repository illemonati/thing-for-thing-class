class Dial {
    constructor(radius = 20, min = 0, max = 360, stepSize = 10) {
        this.radius = radius;
        this.min = min;
        this.max = max;
        this.stepSize = stepSize;
        this.step = min;
    }
    rotate(stepCount = 1) {
        // rotate stepCount amount
    }
    async draw(element) {
        // draw dial inside of what element
        const dialDiv = document.createElement('div');
        const diameter = `${this.radius * 2}px`;
        dialDiv.style.width = diameter;
        dialDiv.style.height = diameter;
        dialDiv.style.backgroundColor = 'black';
        dialDiv.style.borderRadius = '50%';
        element.appendChild(dialDiv);
    }
}
export default Dial;
