rm -r ./build/

cp ./wasm/wasm-image-utils/pkg/*.ts ./src/
cp ./wasm/wasm-image-utils/pkg/*.js ./src/
cp ./wasm/wasm-image-utils/pkg/*.wasm ./src

tsc
cp -r ./templates/* ./build/
cp ./src/*.wasm ./build/

