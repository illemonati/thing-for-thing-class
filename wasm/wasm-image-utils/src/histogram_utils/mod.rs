use wasm_bindgen::prelude::*;
use std::collections::HashMap;

#[wasm_bindgen]
pub fn get_pixel_count_for_color(color: u8, data: &[u8], len: usize) -> Vec<usize> {
    let mut res = vec![0usize; 256];
    for i in ((color as usize)..len).step_by(4) {
        res[data[i] as usize] += 1;
    }
    res
}

#[wasm_bindgen]
pub struct MostUsedColor {
    color: Vec<u8>,
    pub times: usize
}

#[wasm_bindgen]
impl MostUsedColor {
    #[wasm_bindgen(getter)]
    pub fn get_color(&self) -> Vec<u8> {
        return self.color.clone();
    }
}


#[wasm_bindgen]
pub fn get_most_used_color(data: &[u8], len: usize) -> MostUsedColor {
    let mut colors = HashMap::new();
    for i in (0..len).step_by(4) {
        let r = data[i];
        let g = data[i+1];
        let b = data[i+2];
        let color = vec![r, g, b];
        colors.entry(color)
            .and_modify(|e| (*e += 1usize))
            .or_insert(1usize);
    }
    let k = vec![0u8];
    let most_used = colors.iter().fold((&k, &0usize), |most, ele| {
        if ele.1 > most.1 {
            ele
        } else {
            most
        }
    });

    MostUsedColor {
        color: most_used.0.to_vec(),
        times: *most_used.1
    }


}


