
use wasm_bindgen::prelude::*;
use crate::utils::*;
use std::cell::RefCell;
use std::rc::Rc;


#[derive(Copy, Clone, Debug, PartialEq, Eq)]
struct Color {
    r: u8,
    g: u8,
    b: u8,
}

impl Color {
    fn new(r: u8, g: u8, b: u8) -> Self {
        Color {
            r, g, b
        }
    }
    fn get_squared_distance(&self, other: &Self) -> usize {

        let sr = self.r as isize;
        let sg = self.g as isize;
        let sb = self.b as isize;

        let or = other.r as isize;
        let og = other.g as isize;
        let ob = other.b as isize;

        (square!((sr - or)) + square!((sg - og)) + square!((sb - ob))) as usize
    }
}


#[derive(Debug, Clone)]
struct Point {
    x: usize,
    y: usize,
    color: Option<Color>,
    image_data: Rc<RefCell<Vec<u8>>>,
    image_data_len: usize,
    image_width: usize,
    image_height: usize,
    index: usize,
}

impl Point {
    pub fn new(x: usize, y: usize, image_data: Rc<RefCell<Vec<u8>>>, image_width: usize, image_height: usize, image_data_len: usize) -> Self {
        let index = ((y * image_width) + x) * 4;
        let mut p = Point {
            x,
            y,
            color: None,
            image_data,
            image_data_len,
            image_width,
            image_height,
            index
        };
        p.init_color();
        return p;
    }

    fn init_color(&mut self) {
        let image_data = self.image_data.borrow();
        self.color = Some(Color {
            r: image_data[self.index],
            g: image_data[self.index+1],
            b: image_data[self.index+2]
        });
    }

    fn set_color(&mut self, new_color: Color) {
        self.color = Some(new_color);
        let mut image_data = self.image_data.borrow_mut();

        image_data[self.index] = new_color.r;
        image_data[self.index+1] = new_color.g;
        image_data[self.index+2] = new_color.b;
    }

    fn get_offset_point(&mut self, dx: isize, dy: isize) -> Option<Self> {

        set_panic_hook();

        let new_x = self.x as isize + dx;
        let new_y = self.y as isize + dy;

        if new_x < 0 || new_x > self.image_width as isize || new_y < 0 || new_y > self.image_height as isize {
            return None;
        }
        let index = ((new_x * new_y) + new_x) * 4;

        if index >= (self.image_data_len) as isize{
            return None;
        }


        Some(Point::new(new_x as usize, new_y as usize, self.image_data.clone(), self.image_width, self.image_height, self.image_data_len))
    }

    fn get_neighbors(&mut self) -> Vec<Self> {
        let neighbors = vec![

            self.get_offset_point(-1, -1), self.get_offset_point(0, -1), self.get_offset_point(1, -1),
            self.get_offset_point(-1, 0),                                self.get_offset_point(1, 0),
            self.get_offset_point(1, -1), self.get_offset_point(1, 0),   self.get_offset_point(1, 1)
        ];

        let mut real_neighbors = vec![];
        for neighbor in neighbors {
            if neighbor.is_some() {
                real_neighbors.push(neighbor.unwrap());
            }
        }
        real_neighbors
    }


}

#[wasm_bindgen]
pub fn flood_fill(point_x: usize, point_y: usize, tolerence: usize, image_data: Vec<u8>, out_data: &mut [u8], length: usize, image_width: usize, image_height: usize, r: u8, g: u8, b: u8) {
    set_panic_hook();

    if point_x > image_width || point_y > image_height {
        return;
    }

    let image_data = Rc::from(RefCell::new(image_data));
    let mut init_point = Point::new(point_x, point_y, image_data.clone(), image_width, image_height, length);
    let color = Color::new(r, g, b);
    let init_color = init_point.color.unwrap();
    init_point.set_color(color);
    let mut neighbors = vec![init_point.clone()];
    while neighbors.len() > 0 {
        let mut new_neighbors = Vec::new();
        for mut neighbor in neighbors {
            let neighborneightbors = neighbor.get_neighbors();
            for mut neighborneightbor in neighborneightbors {
                if (neighborneightbor.color.unwrap().get_squared_distance(&init_color) < tolerence) && neighborneightbor.color.unwrap() != color {
                    neighborneightbor.set_color(color);
                    new_neighbors.push(neighborneightbor);
                }
            }
        }
        neighbors = new_neighbors;
    }

    for i in 0..length {
        out_data[i] = image_data.borrow()[i];
    }

}




#[wasm_bindgen]
pub fn cool_fill(point_x: usize, point_y: usize, tolerence: usize, image_data: Vec<u8>, out_data: &mut [u8], length: usize, image_width: usize, image_height: usize, r: u8, g: u8, b: u8) {
    set_panic_hook();

    if point_x > image_width || point_y > image_height {
        return;
    }

    let image_data = Rc::from(RefCell::new(image_data));
    let mut init_point = Point::new(point_x, point_y, image_data.clone(), image_width, image_height, length);
    let mut which = true;
    let color = Color::new(r, g, b);
    let color2 = Color::new(255 - r, 255-g, 255-b);
    let init_color = init_point.color.unwrap();
    init_point.set_color(color);
    let mut neighbors = vec![init_point.clone()];
    while neighbors.len() > 0 {
        let mut new_neighbors = Vec::new();
        for mut neighbor in neighbors {
            let neighborneightbors = neighbor.get_neighbors();
            for mut neighborneightbor in neighborneightbors {
                if (neighborneightbor.color.unwrap().get_squared_distance(&init_color) < tolerence) && neighborneightbor.color.unwrap() != color && neighborneightbor.color.unwrap() != color2 {
                    if which {
                        neighborneightbor.set_color(color);
                    } else {
                        neighborneightbor.set_color(color2);
                    }
                    which = !which;
                    // neighborneightbor.set_color(color);
                    new_neighbors.push(neighborneightbor);
                }
            }
        }
        neighbors = new_neighbors;
    }

    for i in 0..length {
        out_data[i] = image_data.borrow()[i];
    }

}




